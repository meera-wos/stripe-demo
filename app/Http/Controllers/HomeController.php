<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cartalyst\Stripe\Stripe;
use PhpParser\Node\Expr\Ternary;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function stripe_connect(Request $request)
    {
        // dd("hii");
        //    $stripe = new \Stripe\StripeClient(
        //     'sk_test_pAIEOGJONoqJxRB9rOFEoIq6'
        //   );
        //dd($stripe);
        //  sk_test_pAIEOGJONoqJxRB9rOFEoIq6
        // pk_test_qKHwL7RicRGaUJm3XTUDRGiB        
        // rk_test_51E5R0wJpIbYB4POQhouYCljzowcQen7fgL5VLgWCLuDX3uuhIMcTT53DxtSM8Y4jdoVg0K2DeCzRHdLIiLkUsesl00D2B5ZtVx
        //    $stripe =  \Stripe\Account::createLoginLink('acct_1E5R0wJpIbYB4POQ');
        // dd($stripe);
        $abc =  \Stripe\Stripe::setApiKey('sk_test_pAIEOGJONoqJxRB9rOFEoIq6');
        $account = \Stripe\Account::create([
            'country' => 'US',
            'type' => 'express',
            'email'=>$request->email,
            'capabilities' => [
            'card_payments' => [
                'requested' => true,
            ],
            'transfers' => [
                'requested' => true,
            ],
            ],
        ]);
            //    dd($account['id']);
            //   $payment_intent = \Stripe\PaymentIntent::create([
            //     'payment_method_types' => ['card'],
            //     'amount' => 1000,
            //     'currency' => 'usd',
            //     'application_fee_amount' => 123,
            //     'transfer_data' => [
            //       'destination' => '{{acct_1E5R0wJpIbYB4POQ}}',
            //     ],
            //   ]);
        $newacount=$account['id'];
            //  dd($payment_intent);
            // dd("hii");
        $account_links = \Stripe\AccountLink::create([
            // 'transfer_data' => [
            //     'destination' => '{{acct_1E5R0wJpIbYB4POQ}}',
            //   ],
            'account' => $newacount,
            'refresh_url' => 'http://localhost/stripDemo/reauth',
            'return_url' => 'http://localhost/stripDemo/return',
            'type' => 'account_onboarding',
            //'collect' => 'eventually_due',
        ]);
        return redirect($account_links['url']);
        // dd($account_links);
    }

    public function returnStrip(Request $request)
    {
        return redirect('/');
    }

    public function reauthStrip(Request $request)
    {
        return redirect('/');
    }
    
}
