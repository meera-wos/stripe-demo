@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{URL::to('/Stripe_connect')}}" method="post">
            @csrf
                <div class="col-4">
                    <input type="email" id="email" name="email" placeholder="Enter Your email id">
                </div>
                <br>
                <div class="col-4">
                    <!-- <a href="javascript:void(0);" onClick="window.open('https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://connect.stripe.com/hosted/oauth&client_id=ca_IECTGTZHLtFXGWWT8JA1mxEjXyIVejAm&state=onbrd_ISa1x4X4ErIQgLVYQxydnKkAth&stripe_user[country]=US')" class="btn btn-danger">Stripe with Connect</a> -->
                    <button type="submit" class="btn btn-danger stripe_connect">Stripe with Connect</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
// $(document).on('click','.stripe_connect',function(){
//   //  var stripe = Stripe('pk_test_qKHwL7RicRGaUJm3XTUDRGiB');
//   //  console.log(stripe);
//    $.ajax({
    
//        type : 'GET',
//        url : "{{URL::to('stripe/Stripe_connect')}}",
//        success:function(data)
//        {
           
//        }
//    })
// });
</script>
@endsection
